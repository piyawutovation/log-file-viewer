<?php


namespace App\Helpers;

/**
 * 
 * a helper file relating file operations
 * 
 */ 

class FileHelper {
    /**
     * 
     * Count number of lines of a file.
     * The speed is pretty optimized using the substr_count command.
     * 
     * Test Result :
     * Uses only 0.5 second for 1GB files
     * 
     * @param string $filepath path to the file
     * 
     * @return int number of lines of the file
     * 
    */
    
    public static function file_line_count($filepath) {
        $line_count = 1;
        $f = fopen($filepath,'r');
        while (! feof($f)) {
            $buffer = fread($f, 8192);
            $line_count += substr_count($buffer,"\n");
        }
        fclose($f);
        return $line_count;
    }
}