<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use App\LogFile;

/**
 * 
 * Backend controller for viewing log files
 * 
 * Support large files with large lines (something like 1GB+ a line) by splitting a query into multiple requests with small sized chunk to process each
 * 
 * @author Sam
 * 
 */

class LoggingController extends Controller
{
    /**
     * 
     * Query for lines in a page
     * 
     * We support long lines by processing partially at a time and the client (JavaScript) can resume the operation by new requests. By processing just partially at a time, each request will use only a limited amount of memory and processing time.
     * 

     * 
     * 
     * HTTP Request Parameters :
     * - @property string filepath the path to the file (relative to Laravel base path)
     * - @property integer start_pos the starting position in the file to process
     * - @property integer line_number the starting line_number
     *
     * @see LogFile::READ_LIMIT_COUNT setting the processing size at a time
     * @see LogFile::readAt() at "return type" section for definition of "Line"
     * 
     *
     * 
     * @return array a JSON string representing query result  
     * 
     *      @property integer 'file_size' the size of the file. needed for the client for calculating the last page.
     *          present only in the first page query or else NULL
     * 
     *      @property integer 'file_line_count' the total number of lines in a file. needed for the client for calculating the last page.
     *          present only in the first page query or else NULL
     * 
     *      @property string 'filepath' the path of the file
     *      @property string 'status' possible values :
     *          - "complete" the query is completed
     *          - "incomplete" the query is processed partially and end prematurely and we must continue operation in new requests until complete
     *          - "error" there is an error occur
     *      @property string 'error' the error message. present if "status" is "error".
     * 
     *      @property array 'prev_page' associative array represent configuration to "previous page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'next_page' associative array represent configuration to "next page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'lines' the extracted lines
     *      
     * 
     */
    
    
    public function anyReadRaw() {
        $validator = Validator::make(app('request')->all(), [
            'start_pos' => 'required|numeric',
            'line_number' => 'required|numeric',
            'filepath' => 'required',
        ]);

        if ($validator->fails()) {
            return ['status'=>'error', 'error'=>$validator->errors()];
        }
        
        $filepath = app('request')->input('filepath');
        $fullfilepath = realpath(base_path($filepath));
        
        if ($fullfilepath === false) {
            return [
                'status' => 'error',
                'error' => 'invalid_file_path',
                ];
        }
        
        if (! file_exists($fullfilepath) || is_dir($fullfilepath)) {
            return [
                'status' => 'error',
                'error' => 'notfound',
                ];
        }
        
        $start_pos = (int) app('request')->input('start_pos');
        
        $line_number = (int) app('request')->input('line_number');
        
        
        
        $data = LogFile::readPage($filepath, $fullfilepath, $start_pos, $line_number);
        
        return $data;
        
    }
    
    /**
     * 
     * Trying to query for lines in a "previous page" relating to current page position
     * 
     * We don't know that the starting position ("pos") is enough to extract enough lines to fill an entire "previous page"
     * because lines come at arbitrary positions and some lines can be too long to fit in our processing length. We just try a length of some value first.
     * If that's not long enough, the client will retry with longer length
     * 
     * The sequence of retrying processing length growth is at exponential rate i.e. x3 every time
     * We will reach any correct length within O(log N) of retries.
     * 
     * The retrying mechanism is controlled by the client (JavaScript).
     * 
     * 
     * HTTP Request Parameters :
     * - @property string filepath the path to the file (relative to Laravel base path)
     * - @property integer pos the starting position in the file to process
     * - @property integer end_pos stop processing when hitting this position in the file
     * - @property integer line_number the starting after the end_pos
     *
     * 
     * @see LogFile::readAt() at "return type" section for definition of "Line"
     * 
     *
     * 
     * @return array a JSON string representing query result  
     * 
     *      @property string 'status' possible values :
     *          - "complete" the query is completed
     *          - "incomplete" the query is processed partially and end prematurely and we must continue operation in new requests until complete
     *          - "error" there is an error occur
     *      @property string 'error' the error message. present if "status" is "error".
     * 
     *      @property array 'next_page' associative array represent configuration to "next page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'lines' the extracted lines
     *      
     * 
     */
    
    
    public function anyReadBack() {
        $validator = Validator::make(app('request')->all(), [
            'pos' => 'required|numeric',
            'end_pos' => 'required|numeric',
            'line_number' => 'required|numeric',
            'filepath' => 'required',
        ]);

        if ($validator->fails()) {
            return ['status'=>'error', 'error'=>$validator->errors()];
        }
        
        $filepath = app('request')->input('filepath');
        $fullfilepath = realpath(base_path($filepath));
        
        
        if ($fullfilepath === false) {
            return [
                'status' => 'error',
                'error' => 'invalid_file_path',
                ];
        }
        
        if (! file_exists($fullfilepath) || is_dir($fullfilepath)) {
            return [
                'status' => 'error',
                'error' => 'notfound',
                ];
        }
        
        
        $pos = (int) app('request')->input('pos');
        
        $end_pos = (int) app('request')->input('end_pos');
        
        $line_number = (int) app('request')->input('line_number');
        
        
        
        
        
        $data = LogFile::readBack($filepath, $fullfilepath, $pos, $end_pos, $line_number);
        
        return $data;
    }
    
    
    
    
    
    

    
    
    

}

