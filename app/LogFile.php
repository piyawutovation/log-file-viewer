<?php

namespace App;

use App\Helpers\FileHelper;

/**
 * 
 * All logic relating log files reading
 * including the split and resume mechanism to support large lines (something like 1GB+ a line)
 * 
 */

class LogFile {
    
    /**
     * 
     * The buffer size function "readAt" process at a time
     * 
     * Set it to NULL for "unlimited"
     * 
     */
    
    const READ_LIMIT_COUNT = 500000;
    
    /**
     * 
     * Number of lines per page
     * 
     */ 
    
    const LINE_PER_PAGE = 10;
    
    /**
     * 
     * Extract lines in a text file
     * 
     * @param string $filepath the path to the file to process
     * @param integer $start_pos the position in the file to start processing
     * @param integer $line_per_page number of lines per a page
     * @param integer $line_count_limit we will stop processing when number of lines processed hit this number.
     *      NULL for "unlimited"
     * @param integer $end_pos stop processing when we hit this position in the file
     * @param bool $circular If true, use circular array instead of normal linear array to store resulting processed lines.
     *      This will limit the memory used.
     * @param integer $line_length_cap Sometimes we don't want to store all of the text in a line to preserve memory. This will cap the length.
     *      NULL for "umlimited"
     * 
     * @return array resulting lines 
     *      Array of associative arrays representing a lines detect
     * 
     *      @property integer 'line_number' the line number
     *      @property string 'text' the text of the line
     *      @property integer 'line_length' the length of the text of the line
     *      @property string 'status' "complete" or "incomplete" in case processing ends prematurely from hitting "end_pos" first
     * 
     */ 
    
    
    public static function readAt($filepath, $start_pos, $line_per_page, $line_count_limit, $end_pos = null, $circular=false, $line_length_cap = null) {
        
        if ($end_pos == null) {
            $end_pos = filesize($filepath);
        }
        
        $f = fopen($filepath,'r');
        fseek($f, $start_pos);
        
        $prev = null;
        $lines = [];
        
        $s = '';
        $line_length = 0;
        
        $line_pos = $start_pos;
        
        $line_count = 0;
        
        $pos = $start_pos;
        $read_count = 0;
        
        $read_count_limit = static::READ_LIMIT_COUNT; // set to null for no cap
        
        $insertLine = function (&$lines, $circular, $text, $pos, $line_number, $line_length, $status, $line_per_page, $line_count) {
            
            $entry = ['text' => $text,  'pos' => $pos, 'line_number'=>$line_number, 'line_length'=>$line_length, 'status'=>$status];
            if (! $circular) {
                $lines[] = $entry;
            }
            else {
                $lines[$line_count % ($line_per_page+1)] = $entry;
            }
        };
        
        for (; ($read_count_limit == null || $read_count < $read_count_limit) && ($line_count_limit == null || $line_count <= $line_count_limit) && (ftell($f) < $end_pos); $read_count += 1) {
            $c = fgetc($f);
            switch ($c) {
                case "\n" :
                    $insertLine($lines, $circular, $s, $line_pos, $line_count+1, $line_length,'complete', $line_per_page, $line_count);
                    $line_count += 1;
                    
                    $s = '';
                    $line_length = 0;
                    $prev = $c;
                    // $line_pos = $pos;
                    $line_pos = ftell($f);
                    break;
                case "\r" :
                    // just ignore it?
                    break;
                default :
                    if ($line_length == 0) {
                        // $line_pos = $pos;
                        $line_pos = ftell($f)-1;
                    }
                    if ($line_length_cap == null || $line_length < $line_length_cap) {
                        $s .= $c;
                    }
                    $line_length += 1;
                    $prev = $c;
            }
            
        }
        
        
        $status = (ftell($f) < $end_pos ? 'incomplete' : 'complete');
        $insertLine($lines, $circular, $s, $line_pos, $line_count+1, $line_length, $status  , $line_per_page, $line_count);
            
        $line_count += 1;
        
        fclose($f);
        
        return $lines;
    }
    
    
    
    
    /**
     * Sometimes in readAt() function, we have to end line processing prematurely
     * and that causes some trailing multi-byte character incomplete
     * and that causes displaying to JSON errors
     * 
     * This function trim those incomplete characters from a processed line
     * 
     * @see LogFile::readAt() Definition of "Line" at "return type" section
     * 
     * @param array the line to be processed
     * 
     * @return array the line with text trimmed
     * 
     * 
     */ 
    
    public static function trim_mb_line($line) {
        while (json_encode($line['text']) === false) {
            
            $line['text'] = substr($line['text'],0, strlen($line['text'])-1);
            if ($line['status'] == 'incomplete') {
                $line['line_length'] -= 1;
            }
        }
        return $line;
    }
    
    
    /**
     * 
     * Query for lines in a page
     * 
     * We support long lines by processing partially at a time and the client (JavaScript) can resume the operation by new requests. By processing just partially at a time, each request will use only a limited amount of memory and processing time.
     * 
     *
     * @see LogFile::READ_LIMIT_COUNT Setting the processing size at a time
     * @see LogFile::readAt() Definition of "Line" at "return type" section
     * 
     * @param string filepath the path to the file (relative to Laravel base path)
     * @param integer start_pos the starting position in the file to process
     * @param integer line_number the starting line_number
     *
     * 
     *
     * 
     * @return array representing query result  
     * 
     *      @property integer 'file_size' the size of the file. needed for the client for calculating the last page.
     *          present only in the first page query or else NULL
     * 
     *      @property integer 'file_line_count' the total number of lines in a file. needed for the client for calculating the last page.
     *          present only in the first page query or else NULL
     * 
     *      @property string 'filepath' the path of the file
     *      @property string 'status' possible values :
     *          - "complete" the query is completed
     *          - "incomplete" the query is processed partially and end prematurely and we must continue operation in new requests until complete
     *          - "error" there is an error occur
     *      @property string 'error' the error message. present if "status" is "error".
     * 
     *      @property array 'prev_page' associative array represent configuration to "previous page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'next_page' associative array represent configuration to "next page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'lines' the extracted lines
     *      
     * 
     */
    
    
    public static function readPage($filepath, $fullfilepath, $start_pos, $line_number) {
        $line_per_page = static::LINE_PER_PAGE;
        
        $last_line_number = floor((($line_number - 1) / $line_per_page) + 1) * $line_per_page;
        
        $line_count_limit = $last_line_number - $line_number + 1;
        
        
        // return [$fullfilepath, $start_pos, $line_per_page, $line_count_limit];
        $lines = static::readAt($fullfilepath, $start_pos, $line_per_page, $line_count_limit);
        // return $lines;
        
        
        $next_pos = -1;
        
        
        $query_status = 'incomplete';
        
        $next_page = null;
        $lines2 = [];
        foreach($lines as $i=>$line) {
            
            $line_line_number = $line['line_number'] - 1 + $line_number;
            
            
            if ($line_line_number <= $last_line_number) {
                $lines2[] = $line;
                $lines2[$i]['line_number'] = $line_line_number;
                $lines2[$i] = static::trim_mb_line($lines2[$i]);
                if ($lines2[$i]['status'] == 'incomplete') {
                    $next_page = [
                        'pos' => $lines2[$i]['pos'] + $lines2[$i]['line_length'],
                        'line_number' => $line_line_number
                        ];
                }
            }
            if ($line_line_number == $last_line_number + 1) {
                $next_page = [
                    'pos' => $line['pos'],
                    'line_number' => $last_line_number + 1
                    ];
                $query_status = 'complete';
            }
        }
        
        
        // case end of file and line count is less than lines per page
        if ($query_status == 'incomplete') {
            // no next page
            if ($lines2[sizeof($lines2)-1]['status'] == 'complete') {
                $query_status = 'complete';
            }
        }
        
        // previos page
        
        
        $prev_page = null;
        
        $file_size = null;
        $file_line_count = null;
        if ($line_number == 1 && $start_pos == 0) {
            $file_size = filesize($fullfilepath);
            $file_line_count = FileHelper::file_line_count($fullfilepath);
        }
        
        
        $data = [
            'file_size' => $file_size,
            'file_line_count' => $file_line_count,
            'filepath' => $filepath,
            'status' => $query_status,
            'prev_page' => $prev_page,
            'next_page' => $next_page,
            'lines' => $lines2,
            
            ];
        
        return $data;
    }
    
    
    /**
     * 
     * Trying to query for lines in a "previous page" relating to current page position
     * 
     * We don't know that the starting position ("pos") is enough to extract enough lines to fill an entire "previous page"
     * because lines come at arbitrary positions and some lines can be too long to fit in our processing length. We just try a length of some value first.
     * If that's not long enough, the client will retry with longer length
     * 
     * The sequence of retrying processing length growth is at exponential rate i.e. x3 every time
     * We will reach any correct length within O(log N) of retries.
     * 
     * The retrying mechanism is controlled by the client (JavaScript).
     * 
     * @see LogFile::readAt() Definition of "Line" at "return type" section
     * 
     * @param string filepath the path to the file (relative to Laravel base path)
     * @param integer pos the starting position in the file to process
     * @param integer end_pos stop processing when hitting this position in the file
     * @param integer line_number the starting after the end_pos
     *
     * 
     * 
     * @return array representing query result  
     * 
     *      @property string 'status' possible values :
     *          - "complete" the query is completed
     *          - "incomplete" the query is processed partially and end prematurely and we must continue operation in new requests until complete
     *          - "error" there is an error occur
     *      @property string 'error' the error message. present if "status" is "error".
     * 
     *      @property array 'next_page' associative array represent configuration to "next page"
     *          - "pos" start position in the file
     *          - "line_number" starting line number in the file
     *      can be NULL if there is no "previous page"
     * 
     *      @property array 'lines' the extracted lines
     *      
     * 
     */
    
    public static function readBack($filepath, $fullfilepath, $pos, $end_pos, $line_number) {
        $line_per_page = LogFile::LINE_PER_PAGE;
        
        $line_this_page = ($line_number - 1) % LogFile::LINE_PER_PAGE;
        if ($line_this_page == 0 && $end_pos > 0) {
            $line_this_page = LogFile::LINE_PER_PAGE;
        }
        
        
        
        $lines = LogFile::readAt($fullfilepath, $pos, $line_this_page, null, $end_pos, true, null);
        
        $lines2 = [];
        foreach($lines as $i=>$line) {
            $lines2[$i] = LogFile::trim_mb_line($line);
        }
        $lines = $lines2;
        
        
        
        usort($lines, function($a,$b) {
           return $a['pos'] - $b['pos'];
        });
        
        
        
        $last_line = null;
        foreach($lines as $line) {
            if ($last_line == null || $line['pos'] > $last_line['pos']) {
                $last_line = $line;
            }
        }
        $next_page = null;
        
        $next_page = [
            'pos' => $last_line['pos'] + $last_line['line_length'],
            ];
        $query_status = ($next_page['pos'] == $end_pos ? 'complete' : 'incomplete');
        
        $lines = array_filter($lines, function($a) use ($end_pos, $fullfilepath) {
            return $a['pos'] < $end_pos || $a['pos'] == $end_pos && $end_pos == filesize($fullfilepath);
        });
        
        if (sizeof($lines) > $line_this_page) {
            $lines = array_slice($lines, sizeof($lines)-$line_this_page);
        }
        
        $data = [
            'next_page' => $next_page,
            'status' => $query_status,
            'lines' => $lines,
            ];
            
        return $data;
        
    }
}