# generate php document
php phpDocumentor.phar -f app/Http/Controllers/LoggingController.php -f app/LogFile.php -f app/Helpers/FileHelper.php -t public/docs/php
jsdoc public/js/main.js -d public/docs/js
vendor/bin/phpunit --coverage-html public/docs/cover