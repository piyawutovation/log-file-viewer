# Log File Viewer (Revision #2)

by Piyawut Pattamanon

Using PHP+Laravel as the backend framework

Vue.js + Bootstrap for front-end part

Designed with large files with large lines (something like 1GB a line) in mind.

Support text files both from Windows and UNIX-like operating systems
(Windows use different newline sequence, CRLF, unlike LF of UNIX-like operating systems)


## 1) Changes

- Make the package cleaner. The size is reduced greatly from the previous revision. Things that can be retrieved using `composer` or `yarn` are excluded from the package.
    - Large test cases (150MBs) are still included though so it's still hundreds of MBs anyway ;)
        - The problem statement states that files are large so why don't use large files to test, right?
- Refactor the backend file structure
    - Move the core logic of log file reading operation from the `LoggingController` controller class to `LogFile` model class so that the controller can keep being thin with minimal logic
    - Move some general file-relating utility functions that are not tied to the logic of `LogFile` class to `FileHelper` helper class e.g. `file_line_count`
    - Note: the utility method `LogFile::trim_mb_line` is tightly tied to the data structure of `LogFile` so it can't be refactored outside
- Add new test cases and make the coverage to 100% for all the important backend classes (i.e. `LoggingController`, `LogFile` and `FileHelper`)
    - One is a Windows text file to be tested
- Unit Testing Coverage Report is included
    - at the directory `public/docs/cover`
    - or at the "Important Files" section
- No algorithm is changed in this revision. I'm glad to modify the algorithm to be more simple if anyone thinks it's too complexed. (I just have designed it very large lines in files as the assumption though and it can be changed depending on situations)

## 2) Algorithms Used

### 2.1) Loading a Page

We load big pages and big lines by splitting into limited-sized chunks across many HTTP requests. This way each HTTP request can operating within limited memory and limited execution time.

### 2.2) Calculating The Position of The Previous Page

Because lines come at random lengths, not at a fixed length, we can't calculate the position of the previous page easily without using brute force scanning and doing that might exceeds execution time limit.

One method to help is to pre-calculate indexes of positions of lines first but that requires having time to do the pre-calculation. The problem statement doesn't state that we can do that and we might not be able to tolerate unresponsiveness at the start so I don't use this method for this problem.

Another method is to do caching of positions of lines we have visited. But the number of lines can be massive and we can't fit all into the memory so I don't use this for this problem too.

The method I use is to jump back from the starting position of the current page by an initial small distance.
From that point, we do scanning lines of the previous page normally, ending at starting position of the current page.
If lines around that zones are too big and rows for the entire previous page can't fit into that range, we retry all over again using a longer distance.

The sequence of retrying backward jumping length grows at an exponential rate i.e. x3 every time.
We will reach any correct length within O(log N) of retries.
I think it's an acceptable time complexity though.


## 3) Installation

Clone the repository first
```
git clone https://piyawutovation@bitbucket.org/piyawutovation/log-file-viewer.git .
```

Build the dependencies of Laravel
```
composer install
```

Build the JS Libraries
(Yarn need to be installed first)
```
cd public
yarn install
```

## 4) Cloud9 Workspace

The cloud workspace is shared and can be accessed by anyone from 
https://ide.c9.io/piyawutovation/pgtest

If we hit "Run Project" button. An Apache server is launched and we can see how the application does live at https://pgtest-piyawutovation.c9users.io

## 5) Sample Input

We can use theses as file name to quickly test

For big lines (1MB-sized lines around page 10+), input this in the path to file text box. The total file size is 145 MB
```
tests/LoggingControllerTest/bigtext.txt
```

For a small file, input this in the path to file text box

```
tests/LoggingControllerTest/smalltext.txt
```

To test a text file from Windows, use this

```
tests/LoggingControllerTest/winfile.txt
```

Note: sorry, the path typed will be based relatively to Laravel base directory, not the `/` system root directory for conveniences


## 6) Running Tests

At command line (in C9 Workspace?), type this
```
/vendor/bin/phpunit
```

## 7) Important Files

### 7.1) Backend


#### 7.1.1) Main Controller File
Source code location
```
app/Http/Controllers/LoggingController.php
```

PHPDoc Generated
```
public/docs/php/classes/App.Http.Controllers.LoggingController.html
```

(Online PHPDoc from C9 Workspace https://pgtest-piyawutovation.c9users.io/docs/classes/App.Http.Controllers.LoggingController.html )

Unit Testing Coverage Result (100%)
![LoggingController Coverage](https://s3-ap-southeast-1.amazonaws.com/pg-logfileviwer/v2/LoggingControllerCoverage.png)

#### 7.1.2) Log File Reading Mechanism Logic

Source code location
```
app/LogFile.php
```

(Online PHPDoc from C9 Workspace https://pgtest-piyawutovation.c9users.io/docs/php/classes/App.LogFile.html )


Unit Testing Coverage Result (100%)
![LoggingController Coverage](https://s3-ap-southeast-1.amazonaws.com/pg-logfileviwer/v2/LogFileCoverage.png)

#### 7.1.3) General File-Relating Utility Helper

Source code location
```
app/Helpers/FileHelper.php
```

(Online PHPDoc from C9 Workspace https://pgtest-piyawutovation.c9users.io/docs/php/classes/App.Helpers.FileHelper.html )

Unit Testing Coverage Result (100%)
![LoggingController Coverage](https://s3-ap-southeast-1.amazonaws.com/pg-logfileviwer/v2/FileHelperCoverage.png)

### 7.2) Front-end

#### 7.2.1) HTML part

Source code location
```
resources/views/welcome.blade.php
```


#### 7.2.2) JS part

Source code location
```
public/js/main.js
```

JSDoc Generated
```
/docs/js/global.html
```

(Online from C9 Workspace https://pgtest-piyawutovation.c9users.io/docs/js/global.html )


### 7.3) Test Cases

Test cases for `LoggingController` class
```
tests/LoggingControllerTest.php
```

Test cases for `LogFile` class
```
tests/LogFileTest.php
```

Test cases for `FileHelper` class
```
tests/FileHelperTest.php
```

#### 7.3.1) Supporting files

Supporting input text file for testing

This is for testing a large file (total file size is 150MB and lines with 1MB of size)
```
tests/LoggingControllerTest/bigtext.txt
```

This is for testing a small file
```
tests/LoggingControllerTest/smalltext.txt
```

This is for Windows text file format testing (Windows uses different "new line" character sequence, CRLF, instead of just LF for UNIX-like operating systems)
```
tests/LoggingControllerTest/winfile.txt
```

Supporting expected output

```
tests/LoggingControllerTest/testAnyReadBack1.json
```
```
tests/LoggingControllerTest/testAnyReadBack2.json
```
```
tests/LoggingControllerTest/testAnyReadRaw1.json
```
```
tests/LoggingControllerTest/testAnyReadRaw2.json
```
```
tests/LoggingControllerTest/testAnyReadRaw1.json
```
```
tests/LoggingControllerTest/testReadAt1.json
```
```
tests/LoggingControllerTest/testReadAt2.json
```
```
tests/LoggingControllerTest/testReadAt3.json
```