<!DOCTYPE html>
<html>
  <head>
    <title>Laravel
    </title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script src="/node_modules/vue/dist/vue.min.js">
    </script>
    <script src="/node_modules/jquery/dist/jquery.min.js">
    </script>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js">
    </script>
    <link href="/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    </link>
  <style>
    body {
      margin: 0;
      padding: 0;
      width: 100%;
      display: table;
      font-weight: 100;
      font-family: 'Lato';
    }
  </style>
  </head>
<body>
  <div class="container">
    <div id="app">
      <h1>@{{ message }}
      </h1>
      <div class="row well well-sm">
        <div class="col-sm-10">
          <input type="text" class="form-control" id="filepath" placeholder="/path/to/file" value="" />
        </div> 
        <!-- left column -->
        <div class="col-sm-2">
          <div class="btn btn-primary center-block" v-on:click="clickView" v-if="loading == null" >
            View
          </div>
          <div class="btn btn-danger center-block" v-if="loading != null" >
            <span class="glyphicon glyphicon-time">
            </span>
            Loading...
          </div>
        </div> 
        <!-- column -->
      </div> 
      <!-- row -->
      <div class="row" v-if="lines != null">
        <div class="col-sm-12">
          <table class="table table-striped">
            <tbody>
              <tr v-for="item in lines">
                <td>
                  @{{ item.line_number }}
                </td>
                <td>
                  @{{ item.text }}
                </td>
              </tr>
            </tbody>
          </table>
        </div> 
        <!-- col -->
      </div> 
      <!-- row -->
      <div class="row well well-sm" v-if="lines != null && loading == null">
        <div class="col-sm-3">
          <div class="btn btn-link center-block" v-on:click="clickFirst" v-if="line_number > 1"  :disabled="loading != null">
            |&lt;
          </div>
        </div>
        <div class="col-sm-3">    
          <div class="btn btn-link center-block" v-on:click="clickBack" v-if="prev_page != null"  :disabled="loading != null">
            &lt;&lt;
          </div>
        </div>
        <div class="col-sm-3">
          <div class="btn btn-link center-block" v-on:click="clickNext" v-if="next_page != null"  :disabled="loading != null">
            &gt;&gt;
          </div>
        </div>
        <div class="col-sm-3">
          <div class="btn btn-link center-block" v-on:click="clickLast" v-if="next_page != null"  :disabled="loading != null">
            &gt;|
          </div>
        </div>
      </div> 
      <!-- navigation row -->
    </div> 
    <!-- app -->
  </div> 
  <!-- container -->
  <script src="/js/main.js">
  </script>
</body>
</html>
