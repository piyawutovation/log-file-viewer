<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use App\Helpers\FileHelper;

class FileHelperTest extends TestCase
{
    
    // ******************* file_line_count *******************
    
    public function testFileLineCountNormal()
    {
        $path = base_path('tests/LoggingControllerTest/bigtext.txt');
        $size = FileHelper::file_line_count($path);
        $this->assertEquals(822,$size);
        
        
        $path = base_path('tests/LoggingControllerTest/smalltext.txt');
        $size = FileHelper::file_line_count($path);
        $this->assertEquals(28,$size);
    }
    
    public function testFileLineCountNotFound() {
        // file should not found
        $this->setExpectedException(\ErrorException::class);
        $path = base_path('nothingherest');
        $size = FileHelper::file_line_count($path);
    }
    
    public function testFileLineCountOutside() {
        // should not be able to access files outside Laravel base directory
        $this->setExpectedException(\ErrorException::class);
        $path = base_path('/var/www/html/index.html <<< ');
        $size = FileHelper::file_line_count($path);
    }
}