<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use App\LogFile;

class LogFileTest extends TestCase
{
    

    /*
    
    ****************** trim_mb_line *******************
    
    */
    
    public function testTrimMbLine() {
        $text = 'สวัสดี';
        $lack1 = substr($text,0,strlen($text)-1);
        $lack2 = substr($text,0,strlen($text)-2);
        $lack3 = substr($text,0,strlen($text)-3);
        
        // lack 1 complete
        
        $line = [
                'text' => $lack1,
                'line_number' => 100,
                'line_length' => 17,
                'status' => 'complete',
                ];
        $result = \App\LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 17,
            'status' => 'complete'
            ], $result);
                
        
        // lack 1 incomplete
                
        $line = [
                'text' => $lack1,
                'line_number'=>100,
                'line_length'=>17,
                'status' => 'incomplete',
                ];
        $result = \App\LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 15,
            'status' => 'incomplete'
            ], $result);
            
            
        // lack 2 complete
        
        $line = [
                'text' => $lack2,
                'line_number' => 100,
                'line_length' => 16,
                'status' => 'complete',
                ];
        $result = LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 16,
            'status' => 'complete'
            ], $result);
                
        
        // lack 2 incomplete
                
        $line = [
                'text' => $lack2,
                'line_number'=>100,
                'line_length'=>16,
                'status' => 'incomplete',
                ];
        $result = LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 15,
            'status' => 'incomplete'
            ], $result);
            
        // lack 3 complete
        
        $line = [
                'text' => $lack2,
                'line_number' => 100,
                'line_length' => 15,
                'status' => 'complete',
                ];
        $result = LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 15,
            'status' => 'complete'
            ], $result);
                
        
        // lack 3 incomplete
                
        $line = [
                'text' => $lack2,
                'line_number'=>100,
                'line_length'=>15,
                'status' => 'incomplete',
                ];
        $result = LogFile::trim_mb_line($line);
        $this->AssertEquals([
            'text' => 'สวัสด',
            'line_number' => 100,
            'line_length' => 14,
            'status' => 'incomplete'
            ], $result);
    }
    
    /*
    ******************* readAt ****************
    */
    
    
    public function testReadAt() {
        $input = [base_path("tests/LoggingControllerTest/bigtext.txt"),145334109,10,10];
        $result = LogFile::readAt($input[0], $input[1],$input[2],$input[3]);
        $expected_json = file_get_contents(base_path('tests/LoggingControllerTest/testReadAt1.json'));
        $expected = json_decode($expected_json, true);
        $this->assertEquals($expected, $result);
        
        $input = [base_path("tests/LoggingControllerTest/bigtext.txt"),0,10,10];
        $result = LogFile::readAt($input[0], $input[1],$input[2],$input[3]);
        $expected_json = file_get_contents(base_path('tests/LoggingControllerTest/testReadAt2.json'));
        $expected = json_decode($expected_json, true);
        $this->assertEquals($expected, $result);
        
        $input = [base_path("tests/LoggingControllerTest/bigtext.txt"),145409453,10,10];
        $result = LogFile::readAt($input[0], $input[1],$input[2],$input[3]);
        $expected_json = file_get_contents(base_path('tests/LoggingControllerTest/testReadAt3.json'));
        $expected = json_decode($expected_json, true);
        $this->assertEquals($expected, $result);
    }
    
    
    
    
}