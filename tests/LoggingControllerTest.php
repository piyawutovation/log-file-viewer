<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use App\LogFile;
use App\Helpers\FileHelper;

class LoggingControllerTest extends TestCase
{
    
    

    // ******************* anyReadAt *********************
    
    
    public function testAnyReadRaw() {
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadRaw1.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fbigtext.txt&line_number=821&start_pos=145409453')
             ->seeJson($expected);
             
             
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadRaw2.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fbigtext.txt&line_number=1&start_pos=0')
             ->seeJson($expected);
             
             
        // incomplete at page 10, need to resume
        // now with 500000 read size
        
        
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testReadRawIncomplete.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fbigtext.txt&line_number=101&start_pos=5851')
             ->seeJson($expected);
             
             
        // test window CRLF
             
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadRawWin1.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fwinfile.txt&line_number=11&start_pos=359')
             ->seeJson($expected);
             
             
        // test invalid path
             
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fbigtext.txtx&line_number=821&start_pos=145409453')
            ->seeJson([
                'status' => 'error',
                'error' => 'invalid_file_path'
            ]);
            
        // test not found
             
        $this->json('GET', '/logging/read-raw?filepath=%2F&line_number=821&start_pos=145409453')
            ->seeJson([
                'status' => 'error',
                'error' => 'notfound'
            ]);
            
        // test omitting line number
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadRawWin1.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', '/logging/read-raw?filepath=tests%2FLoggingControllerTest%2Fwinfile.txt&line_number=&start_pos=359')
             ->seeJson(json_decode('{"status":"error","error":{"line_number":["The line number field is required."]}}', true));
    }
    
    
    
    // ******************** anyReadBack ******************
    
    public function testAnyReadBack() {
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadBack1.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', 'logging/read-back?pos=145399494&end_pos=145409494&filepath=tests%2FLoggingControllerTest%2Fbigtext.txt&line_number=823')
             ->seeJson($expected);
             
             
             
        $expected_json = file_get_contents(base_path("tests/LoggingControllerTest/testAnyReadBack2.json"));
        $expected = json_decode($expected_json, true);
        $this->json('GET', 'logging/read-back?pos=0&end_pos=837&filepath=tests%2FLoggingControllerTest%2Fbigtext.txt&line_number=21')
             ->seeJson($expected);
        
        
        // test omitting line_number
        
        $this->json('GET', 'logging/read-back?pos=0&end_pos=837&filepath=tests%2FLoggingControllerTest%2Fbigtext.txtx&line_number=')
             ->seeJson(json_decode('{"status":"error","error":{"line_number":["The line number field is required."]}}', true));
             
        
        // test invalid path
        
        $this->json('GET', 'logging/read-back?pos=0&end_pos=837&filepath=tests%2FLoggingControllerTest%2Fbigtext.txtx&line_number=21')
             ->seeJson(['status'=>'error','error'=>'invalid_file_path']);
        
        // test not found
        
        $this->json('GET', 'logging/read-back?pos=0&end_pos=837&filepath=%2F&line_number=21')
             ->seeJson(['status'=>'error','error'=>'notfound']);
    }
}
